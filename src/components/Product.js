import * as React from "react";
import PropTypes from "prop-types";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardActionArea from "@mui/material/CardActionArea";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";

import { Link as RouterLink } from "react-router-dom";
import { useDispatch } from "react-redux";
import { selected } from "../features/inventory/slice";

// ** Third Party Components
import { useTranslation } from "react-i18next";

function Product(props) {
  const { product } = props;
  const dispatch = useDispatch();
  // ** Hooks
  const { t } = useTranslation();

  const handleSelected = () => {
    dispatch(selected(product));
  };

  return (
    <Grid item xs={12} md={12}>
      <CardActionArea
        component={RouterLink}
        to={"/request"}
        onClick={handleSelected}
      >
        <Card sx={{ display: "flex" }}>
          <CardContent sx={{ flex: 1 }}>
            <Typography component="h2" variant="h5">
              {product?.name}
            </Typography>
            <Typography variant="subtitle1" paragraph>
              {t(product?.type)}
            </Typography>
            {!props?.detail && (
              <Typography variant="subtitle1" color="primary">
                {t("rent")}
              </Typography>
            )}
          </CardContent>
          <CardMedia
            component="img"
            sx={{ width: 160, display: { sm: "block" } }}
            image={product?.image}
            alt={product?.name}
          />
        </Card>
      </CardActionArea>
    </Grid>
  );
}

Product.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string,
    image: PropTypes.string.isRequired,
  }).isRequired,
  detail: PropTypes.bool,
};

export default Product;
