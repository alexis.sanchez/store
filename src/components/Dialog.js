import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogTitle from "@mui/material/DialogTitle";

// ** Third Party Components
import { useTranslation } from "react-i18next";

export default function AlertDialog(props) {
  // ** Hooks
  const { t } = useTranslation();
  const { setOpen, open, total, handleConfirm } = props;

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {t("TotalAmount")} {total} USD
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleClose}>{t("cancel")}</Button>
          <Button onClick={handleConfirm}>{t("confirm")}</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
