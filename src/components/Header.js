import * as React from "react";
import PropTypes from "prop-types";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";

import Language from "./Language";

function Header(props) {
  const { title } = props;

  return (
    <React.Fragment>
      <Toolbar
        style={{ marginBottom: 50 }}
        sx={{ borderBottom: 1, borderColor: "divider" }}
      >
        <Typography
          component="h2"
          variant="h5"
          color="inherit"
          align="center"
          noWrap
          sx={{ flex: 1 }}
        >
          {title}
        </Typography>
        <Language />
      </Toolbar>
    </React.Fragment>
  );
}

Header.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Header;
