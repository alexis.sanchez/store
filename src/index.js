import React from "react";
import ReactDOM from "react-dom/client";
import { Suspense, lazy } from "react";
import { BrowserRouter } from "react-router-dom";
// ** Fake Database
import "./@fake-db";
// ** i18n
import "./configs/i18n";

import "./index.css";

// ** redux
import { store } from "./app/store";
import { Provider } from "react-redux";

// ** Lazy load app
const LazyApp = lazy(() => import("./App"));
const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Provider store={store}>
        <Suspense fallback={null}>
          <LazyApp />
        </Suspense>
      </Provider>
    </BrowserRouter>
  </React.StrictMode>
);
