// ** Router imports
import { lazy } from "react";

// ** Router imports
import { Routes, Route } from "react-router-dom";

// ** Components
const Home = lazy(() => import("../views/Home"));
const Form = lazy(() => import("../views/Form"));

const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/request" element={<Form />} />
    </Routes>
  );
};

export default Router;
