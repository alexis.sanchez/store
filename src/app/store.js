import { configureStore } from "@reduxjs/toolkit";
import inventoryReducer from "../features/inventory/slice";

export const store = configureStore({
  reducer: {
    inventory: inventoryReducer,
  },
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware({
      serializableCheck: false,
    });
  },
});
