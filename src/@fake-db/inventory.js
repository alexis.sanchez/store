import mock from "./mock";
/* eslint-disable */

const fakeData = [
  {
    id: 0,
    name: "MX 100",
    type: "electric",
    image:
      "https://media.istockphoto.com/id/1415317051/es/foto/bicicleta-el%C3%A9ctrica-negra-aislada-sobre-fondo-blanco.webp?s=612x612&w=is&k=20&c=TEqmRl9uzXkOvW6DZBiI_uVQACeDHZXp9Cx-0bsXr0E=",
  },
  {
    id: 1,
    name: "MXN 200",
    type: "normal",
    image:
      "https://media.istockphoto.com/id/1338461801/es/foto/azul-moderno-hombre-medio-drive-motor-city-touring-o-trekking-e-bicicleta-pedelec-con-motor.webp?s=612x612&w=is&k=20&c=IXR960e9OeOjw-XhvduRBpDC_eyT-o9eWFKC473vqcI=",
  },
  {
    id: 2,
    name: "MXO 300",
    type: "old",
    image:
      "https://media.istockphoto.com/id/667431958/es/vector/bicicleta-el%C3%A9ctrica-aislada-para-deporte-o-paseo-urbano-de-la-ciudad.webp?s=612x612&w=is&k=20&c=K7q8K4uLKeKCz5QsHFebBa8N-XlcGwSRH7QG9wqH9Ro=",
  },
  {
    id: 3,
    name: "MX 110",
    type: "electric",
    image:
      "https://media.istockphoto.com/id/1415317051/es/foto/bicicleta-el%C3%A9ctrica-negra-aislada-sobre-fondo-blanco.webp?s=612x612&w=is&k=20&c=TEqmRl9uzXkOvW6DZBiI_uVQACeDHZXp9Cx-0bsXr0E=",
  },
  {
    id: 4,
    name: "MXN 210",
    type: "normal",
    image:
      "https://media.istockphoto.com/id/1338461801/es/foto/azul-moderno-hombre-medio-drive-motor-city-touring-o-trekking-e-bicicleta-pedelec-con-motor.webp?s=612x612&w=is&k=20&c=IXR960e9OeOjw-XhvduRBpDC_eyT-o9eWFKC473vqcI=",
  },
  {
    id: 5,
    name: "MXO 310",
    type: "old",
    image:
      "https://media.istockphoto.com/id/667431958/es/vector/bicicleta-el%C3%A9ctrica-aislada-para-deporte-o-paseo-urbano-de-la-ciudad.webp?s=612x612&w=is&k=20&c=K7q8K4uLKeKCz5QsHFebBa8N-XlcGwSRH7QG9wqH9Ro=",
  },
  {
    id: 6,
    name: "MX 120",
    type: "electric",
    image:
      "https://media.istockphoto.com/id/1415317051/es/foto/bicicleta-el%C3%A9ctrica-negra-aislada-sobre-fondo-blanco.webp?s=612x612&w=is&k=20&c=TEqmRl9uzXkOvW6DZBiI_uVQACeDHZXp9Cx-0bsXr0E=",
  },
  {
    id: 7,
    name: "MXN 220",
    type: "normal",
    image:
      "https://media.istockphoto.com/id/1338461801/es/foto/azul-moderno-hombre-medio-drive-motor-city-touring-o-trekking-e-bicicleta-pedelec-con-motor.webp?s=612x612&w=is&k=20&c=IXR960e9OeOjw-XhvduRBpDC_eyT-o9eWFKC473vqcI=",
  },
  {
    id: 8,
    name: "MXO 320",
    type: "old",
    image:
      "https://media.istockphoto.com/id/667431958/es/vector/bicicleta-el%C3%A9ctrica-aislada-para-deporte-o-paseo-urbano-de-la-ciudad.webp?s=612x612&w=is&k=20&c=K7q8K4uLKeKCz5QsHFebBa8N-XlcGwSRH7QG9wqH9Ro=",
  },
];
/* eslint-enable */

// ------------------------------------------------
// GET: Return products
// ------------------------------------------------
mock.onGet("/inventory/products").reply(() => {
  return [200, { result: fakeData }];
});
