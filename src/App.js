import React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
// ** Router Import
import Router from "./router";
// ** components
import Header from "./components/Header";
import Footer from "./components/Footer";
// ** Third Party Components
import { useTranslation } from "react-i18next";

const theme = createTheme();

export default function App() {
  const { t } = useTranslation();
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Container maxWidth="lg">
        <Header title={t("BicycleRental")} />
        <Router />
      </Container>
      <Footer title={t("BicycleRental")} description="React + Redux" />
    </ThemeProvider>
  );
}
