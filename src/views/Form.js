/* eslint-disable react-hooks/exhaustive-deps */
import * as React from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import PedalBikeIcon from "@mui/icons-material/PedalBike";
import Typography from "@mui/material/Typography";
import { Link as RouterLink } from "react-router-dom";

import Product from "../components/Product";
import BasicDatePicker from "../components/DatePicker";
import AlertDialog from "../components/Dialog";
import Success from "../components/Success";

// ** store
import { useSelector, useDispatch } from "react-redux";
import { selected } from "../features/inventory/slice";

// ** Third Party Components
import { useTranslation } from "react-i18next";

export default function Form() {
  // ** Hooks
  const { t } = useTranslation();
  const product = useSelector((state) => state.inventory.productDetail);
  const dispatch = useDispatch();
  const [open, setOpen] = React.useState(false);
  const [openSuccess, setOpenSuccess] = React.useState(false);
  const [date, setDate] = React.useState(new Date());
  const [params, setParams] = React.useState({
    fullname: "",
    email: "",
    phone: "",
    date: new Date(),
    days: 1,
  });
  const [total, setTotal] = React.useState(0);

  const PRICE_BASE = 10;
  const PRICE_BASE_AFTER = 12;
  const DAYS_FREE_NORMAL = 3;
  const DAYS_FREE_OLD = 5;

  const calculateTotal = (data) => {
    // verificar si el alquiler inicia antes del 15
    const init = data.date.getDate();
    let priceBase = PRICE_BASE;
    if (init >= 15) {
      priceBase = PRICE_BASE_AFTER;
    }
    // calcular en base al tipo de bicicleta
    if (product.type === "old" && data.days > DAYS_FREE_OLD) {
      priceBase = priceBase * (data.days - DAYS_FREE_OLD) + priceBase;
    } else if (product.type === "normal" && data.days > DAYS_FREE_NORMAL) {
      priceBase = priceBase * (data.days - DAYS_FREE_NORMAL) + priceBase;
    } else if (product.type === "electric") {
      priceBase = priceBase * data.days;
    }
    setTotal(priceBase);
    setOpen(true);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const object = {
      email: data.get("email"),
      fullname: data.get("fullname"),
      phone: data.get("phone"),
      days: parseInt(data.get("days")),
      date,
    };
    setParams((prevState) => ({
      ...prevState,
      ...object,
    }));
    calculateTotal(object);
  };

  const _handleConfirm = () => {
    localStorage.setItem("@formBicycle", JSON.stringify({ ...params, total }));
    setOpen(false);
    setOpenSuccess(true);
  };

  React.useEffect(() => {
    if (!product?.name) {
      const p = JSON.parse(localStorage.getItem("@product"));
      console.log(p);
      if (p?.name) {
        dispatch(selected(p));
      }
    }
  }, []);

  return (
    <Box
      sx={{
        marginTop: 8,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Avatar sx={{ m: 1, bgcolor: "primary.main" }}>
        <PedalBikeIcon />
      </Avatar>
      <Typography component="h1" variant="h5">
        {t("Bicycleapplicationform")}
      </Typography>

      <Box component="form" onSubmit={handleSubmit} sx={{ mt: 1 }}>
        {product?.name ? (
          <Product product={product} detail={true} />
        ) : (
          <Grid container>
            <Grid item xs>
              <Link component={RouterLink} to="/" variant="body2">
                {t("returnList")}
              </Link>
            </Grid>
          </Grid>
        )}

        <TextField
          margin="normal"
          required
          fullWidth
          id="fullname"
          label={t("fullname")}
          name="fullname"
          type="text"
        />
        <TextField
          margin="normal"
          required
          fullWidth
          id="email"
          label={t("email")}
          name="email"
          type="email"
        />
        <TextField
          margin="normal"
          required
          fullWidth
          id="phone"
          label={t("phone")}
          name="phone"
          type="text"
        />
        <BasicDatePicker
          date={date}
          setDate={setDate}
          title={t("rentalStart")}
        />
        <TextField
          margin="normal"
          required
          fullWidth
          id="days"
          label={t("daysRent")}
          name="days"
          type="number"
          min={1}
        />
        <Button
          type="submit"
          fullWidth
          variant="contained"
          sx={{ mt: 3, mb: 2 }}
        >
          {t("rent")}
        </Button>
        <Grid container>
          <Grid item xs>
            <Link component={RouterLink} to="/" variant="body2">
              {t("returnList")}
            </Link>
          </Grid>
        </Grid>
      </Box>

      <AlertDialog
        open={open}
        setOpen={setOpen}
        total={total}
        handleConfirm={_handleConfirm}
      />
      <Success open={openSuccess} setOpen={setOpenSuccess} />
    </Box>
  );
}
