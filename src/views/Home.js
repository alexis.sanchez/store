import React, { useEffect } from "react";
import Grid from "@mui/material/Grid";

// ** components
import Product from "../components/Product.js";

// ** store
import { useSelector, useDispatch } from "react-redux";
import { getProducts } from "../features/inventory/slice";

export default function Home() {
  const list = useSelector((state) => state.inventory.products);
  const dispatch = useDispatch();

  // ** Get products
  useEffect(() => {
    dispatch(getProducts());
  }, [dispatch]);

  return (
    <main>
      <Grid container spacing={5}>
        {list.map((item) => (
          <Product key={item.id} product={item} />
        ))}
      </Grid>
    </main>
  );
}
