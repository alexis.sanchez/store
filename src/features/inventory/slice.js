// ** Redux Imports
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

// ** Axios Imports
import axios from "axios";

export const getProducts = createAsyncThunk(
  "inventory/getProducts",
  async () => {
    const response = await axios.get("/inventory/products");
    return response.data;
  }
);

export const inventorySlice = createSlice({
  name: "inventory",
  initialState: {
    products: [],
    productDetail: {},
  },
  reducers: {
    selected: (state, action) => {
      localStorage.setItem("@product", JSON.stringify(action.payload));
      state.productDetail = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getProducts.fulfilled, (state, action) => {
      state.products = action.payload.result;
    });
  },
});

export const { selected } = inventorySlice.actions;

export default inventorySlice.reducer;
